<?php
/**
 * @file
 * front_page.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function front_page_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: global.
  $config['global'] = array(
    'instance' => 'global',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] | [site:name]',
      ),
      'generator' => array(
        'value' => 'Drupal 7 (http://drupal.org)',
      ),
      'canonical' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'shortlink' => array(
        'value' => '[current-page:url:unaliased]',
      ),
      'og:site_name' => array(
        'value' => '[site:name]',
      ),
      'og:title' => array(
        'value' => '[current-page:title]',
      ),
      'og:type' => array(
        'value' => 'article',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
    ),
  );

  // Exported Metatag config instance: global:403.
  $config['global:403'] = array(
    'instance' => 'global:403',
    'disabled' => FALSE,
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:404.
  $config['global:404'] = array(
    'instance' => 'global:404',
    'disabled' => FALSE,
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:frontpage.
  $config['global:frontpage'] = array(
    'instance' => 'global:frontpage',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[site:name]',
      ),
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
      'og:description' => array(
        'value' => '[site:slogan]',
      ),
      'og:title' => array(
        'value' => '[site:name]',
      ),
      'og:type' => array(
        'value' => 'website',
      ),
      'og:url' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: node.
  $config['node'] = array(
    'instance' => 'node',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[node:title] | [site:name]',
      ),
      'description' => array(
        'value' => '[node:summary]',
      ),
      'article:modified_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
      'article:published_time' => array(
        'value' => '[node:created:custom:c]',
      ),
      'og:description' => array(
        'value' => '[node:summary]',
      ),
      'og:title' => array(
        'value' => '[node:title]',
      ),
      'og:updated_time' => array(
        'value' => '[node:changed:custom:c]',
      ),
    ),
  );

  // Exported Metatag config instance: node:article.
  $config['node:article'] = array(
    'instance' => 'node:article',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:directory_entry.
  $config['node:directory_entry'] = array(
    'instance' => 'node:directory_entry',
    'config' => array(
      'description' => array(
        'value' => '[node:field_directory_description]',
      ),
      'image_src' => array(
        'value' => '[node:field_logo]',
      ),
      'og:description' => array(
        'value' => '[node:field_directory_description]',
      ),
      'og:image' => array(
        'value' => '[node:field_logo],[node:field_logo]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg,image/png',
      ),
    ),
  );

  // Exported Metatag config instance: node:forum.
  $config['node:forum'] = array(
    'instance' => 'node:forum',
    'config' => array(
      'description' => array(
        'value' => '[node:body:value]',
      ),
      'og:description' => array(
        'value' => '[node:body:value]',
      ),
    ),
  );

  // Exported Metatag config instance: node:found_pet.
  $config['node:found_pet'] = array(
    'instance' => 'node:found_pet',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:guest_column.
  $config['node:guest_column'] = array(
    'instance' => 'node:guest_column',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_guestcol_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_guestcol_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:lost_pet.
  $config['node:lost_pet'] = array(
    'instance' => 'node:lost_pet',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:news.
  $config['node:news'] = array(
    'instance' => 'node:news',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_news_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_news_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:node_gallery_item.
  $config['node:node_gallery_item'] = array(
    'instance' => 'node:node_gallery_item',
    'config' => array(
      'image_src' => array(
        'value' => '[node:node_gallery_media]',
      ),
      'og:image' => array(
        'value' => '[node:node_gallery_media]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:opinion.
  $config['node:opinion'] = array(
    'instance' => 'node:opinion',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg,image/png',
      ),
    ),
  );

  // Exported Metatag config instance: node:opinion_invite.
  $config['node:opinion_invite'] = array(
    'instance' => 'node:opinion_invite',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:pet_for_adoption.
  $config['node:pet_for_adoption'] = array(
    'instance' => 'node:pet_for_adoption',
    'config' => array(
      'description' => array(
        'value' => '[node:field_adopted_pet_description]',
      ),
      'image_src' => array(
        'value' => '[node:field_image]',
      ),
      'og:description' => array(
        'value' => '[node:field_adopted_pet_description]',
      ),
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:pet_video.
  $config['node:pet_video'] = array(
    'instance' => 'node:pet_video',
    'config' => array(
      'description' => array(
        'value' => '[node:body:value]',
      ),
      'image_src' => array(
        'value' => '[node:field-petvideo-youtube-link:thumbnail_path]',
      ),
      'og:description' => array(
        'value' => '[node:body:value]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
      'og:video' => array(
        'value' => '[node:field_petvideo_youtube_link]',
      ),
    ),
  );

  // Exported Metatag config instance: node:photo_contest.
  $config['node:photo_contest'] = array(
    'instance' => 'node:photo_contest',
    'config' => array(
      'description' => array(
        'value' => '[node:field_contest_description]',
      ),
      'image_src' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:description' => array(
        'value' => '[node:field_contest_description]',
      ),
      'og:image' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:photo_contest_entry.
  $config['node:photo_contest_entry'] = array(
    'instance' => 'node:photo_contest_entry',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg',
      ),
    ),
  );

  // Exported Metatag config instance: node:video_contest.
  $config['node:video_contest'] = array(
    'instance' => 'node:video_contest',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:image' => array(
        'value' => '[node:field_contest_image]',
      ),
      'og:image:type' => array(
        'value' => 'image/jpeg,image/png',
      ),
    ),
  );

  // Exported Metatag config instance: node:video_contest_entry.
  $config['node:video_contest_entry'] = array(
    'instance' => 'node:video_contest_entry',
    'config' => array(),
  );

  // Exported Metatag config instance: taxonomy_term.
  $config['taxonomy_term'] = array(
    'instance' => 'taxonomy_term',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[term:name] | [site:name]',
      ),
      'description' => array(
        'value' => '[term:description]',
      ),
      'og:description' => array(
        'value' => '[term:description]',
      ),
      'og:title' => array(
        'value' => '[term:name]',
      ),
    ),
  );

  // Exported Metatag config instance: user.
  $config['user'] = array(
    'instance' => 'user',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '[user:name] | [site:name]',
      ),
      'og:title' => array(
        'value' => '[user:name]',
      ),
      'og:type' => array(
        'value' => 'profile',
      ),
      'profile:username' => array(
        'value' => '[user:name]',
      ),
      'og:image' => array(
        'value' => '[user:picture:url]',
      ),
    ),
  );

  return $config;
}
